package instrukcje.warunkowe;

public class WzajemnieWykluczajace {
    public static void main(String[] args) {

        int zmienna = 0;

        // pierwszy warunek jest spełniony i pierwsza zawartość "if" zostanie wykonana
        // kolejne warunki if NAWET NIE ZOSTANĄ SPRAWDZONE (INKREMENTACJA W DRUGIM IF NIE ZOSTANIE WYKONANA).
        if (zmienna < 1) { // tak
            System.out.println("Mniejsza od 1.");
        } else if (zmienna++ > -3) {// tak
            System.out.println("Większa od -3.");
        }

        System.out.println(zmienna); // 0


        // Oba warunki są niezależnie sprawdzane.
        if (zmienna < 1) { // tak
            System.out.println("Mniejsza od 1.");
        }
        if (zmienna > -3) {// tak
            System.out.println("Większa od -3.");
        }
    }
}
