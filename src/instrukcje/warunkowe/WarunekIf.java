package instrukcje.warunkowe;

public class WarunekIf {
    public static void main(String[] args) {
        if (2 > 3) {
            System.out.println(":)");
        } else {
            System.out.println(":(");
        }

        if (4 < 5) {
            System.out.println(":)");
        }else {
            System.out.println(":(");
        }

        if ((2 - 2) == 0) {
            System.out.println(":)");
        }else {
            System.out.println(":(");
        }

        if (true) {
            System.out.println(":)");
        }else {
            System.out.println(":(");
        }

        if (9 % 2 == 0) { // reszta z dzielenia 9/2
            System.out.println(":)");
        }else {
            System.out.println(":(");
        }

        if (9 % 3 == 0) {
            System.out.println(":)");
        }else {
            System.out.println(":(");
        }
    }
}
