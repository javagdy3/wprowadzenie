package instrukcje.wczytywanie;

import java.util.Random;

public class RandomDemo {
    public static void main(String[] args) {
        Random generator = new Random();

        int losowyInt = generator.nextInt();

        System.out.println("Wylosowana liczba: " + losowyInt);

        int losowaLiczbaOd125Do136 = generator.nextInt(11) + 125; // zakres 125 - 136
        int losowaLiczbaDo100 = generator.nextInt(100);
        int losowaLiczbaOd50Do60 = generator.nextInt(10) + 50;

        int liczbaDo100 = generator.nextInt(100); // losuję 10 % prawdopodobieństwo.
        if (liczbaDo100 < 10) {
            System.out.println("Tak");
        }


    }
}
