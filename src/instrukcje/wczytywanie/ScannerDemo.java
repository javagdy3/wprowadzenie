package instrukcje.wczytywanie;

import java.util.Scanner;

public class ScannerDemo {
    public static void main(String[] args) {
        Scanner wejscie; // deklaracja obiektu Scanner. Nazwa zmiennej = wejscie

        wejscie = new Scanner(System.in);


        System.out.println("Podaj komende:");
        String komenda = wejscie.nextLine();

        System.out.println(komenda);
//        "dodaj mało 2.30 23%vat"
//        "dodaj" "masło" "2.30" "23%vat"
//        komenda.split(" ");


        System.out.println("Wpisz linię tekstu:");
        String liniaTekstu = wejscie.nextLine();
        System.out.println("Wpisałeś/łaś: " + liniaTekstu);

        System.out.println("Wpisz coś jeszcze:");
        String slowoWLinii = wejscie.next(); // jeden token, jedno słowo
        System.out.println("Wpisałeś/łaś: " + slowoWLinii);

        slowoWLinii = wejscie.next(); // jeden token, jedno słowo
        System.out.println("Wpisałeś/łaś: " + slowoWLinii);

        liniaTekstu = wejscie.nextLine(); // jeden token, jedno słowo
        System.out.println("Wpisałeś/łaś: " + liniaTekstu);


        System.out.println("Podaj liczbę do 100");
        int liczba = wejscie.nextInt();
        System.out.println("Twoja liczba to: " + liczba);
        liniaTekstu = wejscie.nextLine(); // jeden token, jedno słowo
        System.out.println("Wpisałeś/łaś: " + liniaTekstu);


        System.out.println("Podaj liczbę do 100");
        String token = wejscie.nextLine();
        int liczbaParsowana = Integer.parseInt(token); // zmiana tekstu na liczbę całkowitą

        liniaTekstu = wejscie.nextLine(); // jeden token, jedno słowo
        System.out.println("Wpisałeś/łaś: " + liniaTekstu);

//         ROZWIĄZANIE:
//        - opcja A: stosujemy next, nextInt, nextDouble, nextFloat, nextChar ... itd. naprzemiennie (pobiera tokeny)
//        - opcja B: stosujemy tylko nextLine i w razie potrzeby załadowania liczby, używamy parserów z klas :
//                          - Integer.parseInt(tekst);

    }
}
