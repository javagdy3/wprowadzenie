package instrukcje.petle;

public class PetlaWhile {
    public static void main(String[] args) {
        int j = 0;

        while (j < 100) {
            System.out.println((1 + j) + ". hej");

            j+=2;
        }

        // to co wyżej, jest to samo
        for (int i = 0; i < 100; i+=2) {
            System.out.println((1 + i) + ". hej");
        }
    }
}
