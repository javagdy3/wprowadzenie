package instrukcje.petle;

public class PetlaFor {
    public static void main(String[] args) {

//      for (inicjaliz; warun; inkrem){
//        inicjalizacja występuje raz
//        warunek -sprawdzany przed każdym obiegiem
//        inkrementacja - po każdym obiegu
        for (int i = 0; i < 100; i++) {
            System.out.println((i + 1) + ". hej");
            // 0 = tak hej
            // 1 = tak hej
            // 2 = tak hej
            // 3 = tak hej
            // 4 = tak hej
            // 5 = nie
        }
        System.out.println("Koniec");
    }
}
