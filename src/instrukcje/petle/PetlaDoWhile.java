package instrukcje.petle;

import java.util.Scanner;

public class PetlaDoWhile {
    public static void main(String[] args) {

        // Tworzę scanner aby wczytywać dane od użytkownika
        Scanner scanner = new Scanner(System.in);

        // deklaruję zmienną komenda - dlaczego tu? dlaczego bez wartości?
        String komenda;
        do {
            System.out.println("Wpisz cześć:");
            komenda = scanner.nextLine(); // wczytuje linię

            if (komenda.equals("cześć")) {
                System.out.println("no hej!");
            }

            // dopóki?
        } while (!komenda.equals("quit"));
    }
}
