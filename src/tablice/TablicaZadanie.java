package tablice;

import metody.MetodaDemo;

public class TablicaZadanie {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 3, 5, 10};

        // i            - to indeks
        // tablica[i]   - to wartość
        for (int i = 0; i < tablica.length; i++) {
//            if (i % 2 == 0) {
            if (MetodaDemo.czyLiczbaJestParzysta(i)) {
                System.out.println(tablica[i]);
            }
        }

        for (int i = 0; i < tablica.length; i++) {
//            if (tablica[i] % 2 == 0) {
            if (MetodaDemo.czyLiczbaJestParzysta(tablica[i])) {
                System.out.println(tablica[i]);
            }
        }

        System.out.println();
        // tablica[0] = 1
        // tablica[1] = 3
        // tablica[2] = 5
        // tablica[3] = 10

        // tablica wypisana od tyłu
        for (int i = tablica.length - 1; i >= 0; i--) {
            System.out.println(tablica[i]);
        }

        System.out.println();
        // tablica ma rozmiar 4
        // i będzie miało wartości 0 1 2 3
        // 4 - 0 - x = 3
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[(tablica.length - 1 - i)]);
        }
    }
}
