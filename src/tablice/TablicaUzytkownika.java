package tablice;

import metody.MetodaDemo;

import java.util.Arrays;
import java.util.Scanner;

public class TablicaUzytkownika {
    public static void main(String[] args) {
        // wczytaj od uzytkownika rozmiar tablicy N
        // wczytaj od uzytkownika N liczb i wpisz je do tablicy
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rozmiar tablicy:");
        int rozmiarN = scanner.nextInt();

        // tworzymy tablicę o rozmiarze podanym przez uzytkownika
        int[] tablica = new int[rozmiarN];

        for (int i = 0; i < tablica.length; i++) {
            System.out.println("Podaj liczbę w komórce " + i + ": ");
            int nowaZmienna = scanner.nextInt();

            tablica[i] = nowaZmienna;
        }

        // Arrays.toString() - metoda która iteruje się przez elementy i wypisuje je
        // możemy tej metody używać zamiast pisać własną pętle wypisującą elementy.
        System.out.println(Arrays.toString(tablica));
        MetodaDemo.wypiszElementyTablicy(tablica);
    }
}
