package tablice;

import metody.MetodaDemo;

public class TablicaDemo {
    public static void main(String[] args) {
        int[] tablica = new int[]{30, 50, 40, 90, 100};

//        tablica[0] = 30;
//        tablica[1] = 50;
//        tablica[2] = 40;
//        tablica[3] = 90;
//        tablica[4] = 100;

        System.out.println(tablica);

        // wypisanie tablicy
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }

        MetodaDemo.wypiszElementyTablicy(tablica);

        // szukanie maksimum
        int max = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
        }
        System.out.println("Maksimum: " + max);

        // Twoje zadanie - zaimplementuj pętlę znajdującą minimum
        // szukanie minimum
        int minimum = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] < minimum) {
                minimum = tablica[i];
            }
        }
        System.out.println("Minimum: " + minimum);
    }
}
