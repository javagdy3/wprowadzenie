package zadanie.zmienne;

public class Zadanie {
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        int c = 3;

        int tymczasowa = a;

        a = b;
        b = c;
        c = tymczasowa;

        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
