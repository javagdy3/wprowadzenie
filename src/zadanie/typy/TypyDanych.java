package zadanie.typy;

public class TypyDanych {
    public static void main(String[] args) {
        System.out.println("Operacja (2+3) = " + (2 + 3));
        //                                    ^ konkatenacja
        //                                         ^ dodawanie

        System.out.println("Operacja (\"a\" + 2) = " + ("a" + 2));
        //                            ^^ dodanie znaku " jako część string.
        //                            ^^ pokazujemy kompilatorowi że nie ma traktować znaku " jako kończącego ciąg znaków

        System.out.println("Operacja (5 / 2) = " + 5 / 2);
        System.out.println("Operacja (5.0 / 2) = " + (5.0 / 2));        // DZIAŁANIE MATEMATYCZNE ZOSTANIE WYKONANE Z PRECYZJĄ NAJBARDZIEJ
        // PRECYZYJNEJ SKŁADOWEJ - ponieważ działanie zawiera float,
        //          to wynik będzie float
        System.out.println("Operacja (5 / 2.0) = " + (5 / 2.0));
        System.out.println("Operacja (5 / 2.0) = " + (5 / 2.0));
        System.out.println("Operacja (5.0 / 2.0) = " + (5.0 / 2.0));
        System.out.println("Operacja (100L - 10) = " + (100L - 10));
        System.out.println("Operacja (2f - 3) = " + (2f - 3));
        System.out.println("Operacja (5f / 2) = " + (5f / 2));
        System.out.println("Operacja (5d / 2) = " + (5d / 2));
        System.out.println("Operacja ('A' + 2) = " + ('A' + 2));  // kod ascii 65 + 2 = 67
        System.out.println("Operacja ('a' + 2) = " + ('a' + 2)); // kod ascii 97 + 2 = 99
        System.out.println("Operacja (\"a\" + 2) = " + ("a" + 2));
        System.out.println("Operacja (\"a\" + \"b\") = " + ("a" + "b"));
        System.out.println("Operacja ('a' + 'b') = " + ('a' + 'b'));
        System.out.println("Operacja (\"a\" + 'b') = " + ("a" + 'b'));
        System.out.println("Operacja (\"a\" + \"b\" + 3) = " + ("a" + "b" + 3));
        System.out.println("Operacja ('b' + 3 + \"a\") = " + ('b' + 3 + "a"));
        System.out.println("Operacja (9 % 4) = " + (9 % 4));
        System.out.println("Operacja (9 % 4) = " + (9 - ((9 / 4) * 4)));
        System.out.println("Tutaj sobie wprowadze");

        short tysiac = 1000;
        byte tysionc = (byte) tysiac;
        System.out.println(tysionc);

//        Math.ceil(1.0001); = 2
//        Math.round()
    }
}
