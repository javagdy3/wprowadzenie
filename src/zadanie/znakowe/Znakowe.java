package zadanie.znakowe;

public class Znakowe {
    public static void main(String[] args) {
        String mojeImie = "Paweł";
        String wiek = " ma 10 lat";

        int zmienna = 1;

        System.out.println(mojeImie);
        System.out.println(mojeImie.substring(0, 3));

        int długość = mojeImie.length();

        System.out.println(mojeImie.substring(długość - 3));

        mojeImie.substring(3);

        System.out.println(mojeImie); // Paweł

        // konkatenacja
        System.out.println(mojeImie + wiek);

    }
}
