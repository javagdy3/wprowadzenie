package zadanie.logiczne;

public class Logiczne {
    public static void main(String[] args) {

        boolean prawda = true;
        boolean fałsz = false;

        System.out.println(5 == 4);
        System.out.println(5 != 4);
        System.out.println(false && true); // fałsz
        System.out.println(!true && !false); // fałsz
        System.out.println(!true || !false); // prawda (chociaż jedna musi być prawdziwa
        System.out.println(!true || (5 != 4)); // prawda


        int zmiennaLiczbowa = 5;

        zmiennaLiczbowa++;

        System.out.println(zmiennaLiczbowa); // 6
        System.out.println(zmiennaLiczbowa++); // 6 //post inkrementacja
        System.out.println(zmiennaLiczbowa); // 7

        System.out.println(++zmiennaLiczbowa); // pre inkrementacja //8

        zmiennaLiczbowa -= 2;
        zmiennaLiczbowa = zmiennaLiczbowa - 2; // to samo co wyżej

        zmiennaLiczbowa += 2;
        zmiennaLiczbowa = zmiennaLiczbowa + 2; // to samo co wyżej

        System.out.println("Siema, jestem sobie zmiana!");


        System.out.println(false == false); // true
        System.out.println(false != true); // true
        System.out.println(!true);      // false
        System.out.println(2 > 4);      // false
        System.out.println(3 > 5);      // false
        System.out.println(3 == 3 && 3 == 4); // false
        System.out.println(3 != 5 || 3 == 5); // true
        System.out.println((2 + 4) > (1 + 3)); // true
        System.out.println("cos".equals("cos")); // true
        System.out.println("cos" == "cos"); // true // znaki równości porównują referencję

        String zmienna1 = new String("cos");
//        String zmienna1 = "cos";
        String zmienna2 = new String("cos");

        System.out.println();
        System.out.println(zmienna1.equals(zmienna2)); // porównanie treści zmiennych (zawartości string)
        System.out.println(zmienna1.equalsIgnoreCase(zmienna2)); // porównanie treści zmiennych (zawartości string)
        System.out.println(zmienna1 == zmienna2); // porównanie adresów obiektów (różne adresy)
        System.out.println(zmienna1 == zmienna1); // true


    }
}
