package zadanie.arytmetyczne;

public class Dzielenie {
    public static void main(String[] args) {
        double a;
        int b;
        int c;

        a = 1;
        b = 2;
        c = 3;
//        int d, e, f; // ctrl + '/'
        /*int d, e, f; // ctrl + shift + '/'*/

        System.out.println(a / 2);  // 1 / 2 = (0.5) = 0
        System.out.println(a / b);  // 1 / 2 = (0.5) = 0
        System.out.println(1 / 1);  // 1
        System.out.println(a / (b / c)); // 1/(2/3) = 1 / 0 =

//        c = a / b;

//        System.out.println(c);


    }
}
