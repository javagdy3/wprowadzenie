package metody;

import java.util.Scanner;

public class MetodaDemo {
    public static void main(String[] args) {
        // KISS - keep it simple stupid
        // DRY  - don't repeat yourself
//
//        wypiszElementyTablicy(new int[]{1, 2, 3, 4});
//
//        int wynik = obliczSumeElementow(new int[]{1, 2, 3, 4});
//        System.out.println("Suma to:" + wynik);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Wprowadź liczbę:");
        int cosWpisanegoPrzezUzytkownika = scanner.nextInt();

        drukujParzystośćLiczby(cosWpisanegoPrzezUzytkownika);
    }

    public static boolean czyLiczbaJestParzysta(int liczba){
        if (liczba % 2 == 0) {
            return true;
        }else{
            return false;
        }
    }

    public static void drukujParzystośćLiczby(int liczba) {
        if (liczba % 2 == 0) {
            System.out.println("Liczba jest parzysta.");
        }else{
            System.out.println("Liczba jest nieparzysta.");
        }
    }

    public static int obliczSumeElementow(int[] tablica) {
        int suma = 0;
        for (int i = 0; i < tablica.length; i++) {
            suma = suma + tablica[i];
        }

        return suma;
    }

    public static void wypiszElementyTablicy(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }
    }

}
