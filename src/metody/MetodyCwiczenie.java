package metody;

public class MetodyCwiczenie {
    public static void main(String[] args) {

        znajdzMaxIWypisz(new int[]{1, 2, 3});
        znajdzMaxIWypisz(new int[]{500, 1000, 2});

        int wynikMetody = znajdzMaxIZwroc(new int[]{532, 4365, 1, 23, 62, 46, 3124, 5, 46, 2, 4, 235, 6, 45, 7457, 435, 6, 234, 23, 43, 45, 756, 78, 45, 23, 45});
        System.out.println("Wynik (maksimum): "+ wynikMetody);

        
    }

    public static void znajdzMaxIWypisz(int[] tablica) {
        int max = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
        }
        System.out.println("Maksimum: " + max);
    }

    public static int znajdzMaxIZwroc(int[] tablica) {
        int max = tablica[0];
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
        }
        return max;
    }

    public static int znajdzIneksMaxIZwroc(int[] tablica) {
        int max = tablica[0];
        int maxI = 0;
        for (int i = 1; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
                maxI = i;
            }
        }
        return maxI;
    }
}
